FROM node:4.8

RUN npm install -g oauth-proxy

EXPOSE 8001

ENTRYPOINT ["oauth-proxy"]
