# oauthProxy
https://github.com/mojodna/node-oauth-proxy

port 8001 is exposed for the proxy

## todo
- implement hashicorp vault for secrets
- implement ansible to deploy oauth proxy to server
- implement ansible to create oauth-proxy curl script to simplify usage

### build image
```
cd oauthProxy
docker build -t oauth-proxy .
```

## run oauthProxy in the background

notice the -d in the run command

```
docker run -d --name oauth-proxy -p 8001:8001 oauth-proxy --consumer-key <key> --consumer-secret <secret>
```

## run oauthProxy in the foreground

```
docker run --name oauth-proxy-sitb -p 8001:8001 oauth-proxy --consumer-key <key> --consumer-secret <secret>
```

## use oauth-proxy through curl

```
curl -x localhost:8001 http://host.name/path
```

